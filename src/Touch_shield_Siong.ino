// the regular Adafruit "TouchScreen.h" library only works on AVRs

// different mcufriend shields have Touchscreen on different pins
// and rotation.
// Run the UTouch_calibr_kbv sketch for calibration of your shield

//#include <Adafruit_TFTLCD.h> // Hardware-specific library
//Adafruit_TFTLCD tft(A3, A2, A1, A0, A4);

#include "Adafruit_GFX.h"    // Core graphics library
#include "MCUFRIEND_kbv.h"
#include "TouchScreen.h"
#include "Menu_adafruit.h"
#include "startup_screen.h"

startup_screen start_display;
Menu_adafruit menu_adafruit;

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
//TouchScreen ts = TouchScreen(XP, YP, XM, YM, 250);
TSPoint tp;

MCUFRIEND_kbv tft;       // hard-wired for UNO shields anyway.
MCUFRIEND_kbv displayTFT9327;
uint8_t i=0;

static uint8_t number_module_connected = 1;

#if defined(__SAM3X8E__)
#undef __FlashStringHelper::F(string_literal)
#define F(string_literal) string_literal
#endif

//----------------------------------------|
// TFT Breakout  -- Arduino UNO / Mega2560 / OPEN-SMART UNO Black
// GND              -- GND
// 3V3               -- 3.3V
// CS                 -- A3
// RS                 -- A2
// WR                -- A1
// RD                 -- A0
// RST                -- RESET
// LED                -- GND
// DB0                -- 8
// DB1                -- 9
// DB2                -- 10
// DB3                -- 11
// DB4                -- 4
// DB5                -- 13
// DB6                -- 6
// DB7                -- 7

//

void loop()
{

}


void setup(void)
{
	Serial.begin(9600);
	delay(1000);

	start_display.setNbModuleConnected(number_module_connected);
	start_display.display_startup_screen(displayTFT9327, menu_adafruit);
}


