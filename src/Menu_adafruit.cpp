#include "Menu_adafruit.h"



Menu_adafruit::Menu_adafruit(void)
{
	TS_LEFT = 880;
	TS_RIGHT = 170;
	TS_TOP = 950;
	TS_BOT = 180;
}

void Menu_adafruit::show_Serial(MCUFRIEND_kbv tft)
{
	Serial.print(F("Found "));
	Serial.print("ILITEK 9327");
	Serial.println(F(" LCD driver"));
	Serial.print(F("ID=0x"));
	Serial.println(identifier, HEX);
	Serial.println("Screen is " + String(tft.width()) + "x" + String(tft.height()));
	Serial.println("Calibration is: ");
	Serial.println("LEFT = " + String(TS_LEFT) + " RT  = " + String(TS_RIGHT));
	Serial.println("TOP  = " + String(TS_TOP) + " BOT = " + String(TS_BOT));
	Serial.print("Wiring is: ");
	Serial.println(SwapXY ? "SWAPXY" : "PORTRAIT");
	Serial.println("YP=" + String(YP) + " XM=" + String(XM));
	Serial.println("YM=" + String(YM) + " XP=" + String(XP));
}

void Menu_adafruit::setCoordination(TSPoint tp, uint8_t rotation)
{


}

void Menu_adafruit::getCoordination(long * output_xpos, long * output_ypos, MCUFRIEND_kbv tft, uint8_t rotation)
{
	uint16_t tmp;
	long xpos, ypos;

	while (1)
	{
		// This part detect pressure and display informations on the screen
		tp = ts.getPoint();
		pinMode(XM, OUTPUT);
		pinMode(YP, OUTPUT);
		pinMode(XP, OUTPUT);
		pinMode(YM, OUTPUT);

		if (tp.z > MINPRESSURE && tp.z < MAXPRESSURE)
		{

			switch (rotation) {      // adjust for different aspects
			case 0:   break;        //no change,  calibrated for PORTRAIT
			case 1:   tmp = TS_LEFT;
				TS_LEFT = TS_BOT;
				TS_BOT = TS_RIGHT;
				TS_RIGHT = TS_TOP;
				TS_TOP = tmp;
				break;
			case 2:   SWAP(TS_LEFT, TS_RIGHT);
				SWAP(TS_TOP, TS_BOT);
				break;
			case 3:   tmp = TS_LEFT;
				TS_LEFT = TS_BOT;
				TS_TOP = TS_RIGHT;
				TS_RIGHT = TS_TOP;
				TS_BOT = tmp;
				break;
			}

			xpos = map(tp.x, TS_LEFT, TS_RIGHT, 0, tft.width()); // ts_left = , ts_right = 240
			ypos = map(tp.y, TS_TOP, TS_BOT, 0, tft.height()); // ts_top = 0 , ts_bot = 400

			output_xpos = &xpos;
			output_ypos = &ypos;

			printTFT(tft, 0, (tft.height() * 3) / 4 - 40, 1, YELLOW, BLACK, "tp.x =" + (String)tp.x);
			printTFT(tft, 0, (tft.height() * 3) / 4 - 30, 1, YELLOW, BLACK, "tp.y =" + (String)tp.y);
			printTFT(tft, 0, (tft.height() * 3) / 4 - 20, 1, YELLOW, BLACK, "width =" + (String)tp.z);
			printTFT(tft, 0, (tft.height() * 3) / 4, 1, YELLOW, BLACK, "xpos =" + (String)xpos);
			printTFT(tft, 0, (tft.height() * 3) / 4 + 10, 1, YELLOW, BLACK, "ypos =" + (String)ypos);
		}
	}	
}

void Menu_adafruit::printTFT(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint8_t sizeText, uint16_t color, uint16_t background , String text)
{
	uint8_t sizeCharacter = 8; // one character is 8 pixel

	this->clearLine(tft , x, y, tft.width(), sizeText, background);
	tft.setCursor(x,y);
	tft.setTextColor(color, background);
	tft.print((String)text);

}

void Menu_adafruit::printTFT(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint8_t sizeText, uint16_t color, uint16_t background, const char *text)
{
	uint8_t sizeCharacter = 8; // one character is 8 pixel

	tft.setTextSize(sizeText);
	tft.setCursor(x, y);
	tft.setTextColor(color, background);
	tft.print(text);
}

void Menu_adafruit::clearLine(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint16_t width , uint8_t sizeText , uint16_t color)
{
	tft.fillRect(x, y, width , 8 * sizeText, color);
}

int16_t Menu_adafruit::getIdentifier(void)
{
	return identifier;
}



void Menu_adafruit::displayTFT(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint8_t textSize, uint16_t color, uint16_t background, uint8_t rotation , const char *text)
{
	char * textTest = " Bonjour a vous ";
	uint16_t BOXSIZE;

	uint16_t number=0, numbTest=0;
	uint16_t xpos, ypos;  //screen coordinates

	tft.begin(identifier);		
	tft.setRotation(rotation);
	setRotationTouchScreenXY(rotation);
	tft.fillScreen(background); // background is black color
	tft.setCursor(x, y);
/*	tft.setTextSize(textSize);
	tft.print(text);
	tft.setTextColor(color, background);*/

	tft.setTextSize(1);
	//tft.setCursor((tft.width() / 3), (tft.height() / 2));
	//tft.print(" number =" + number);
	//tft.setTextColor(YELLOW, BLACK);

	uint16_t tmp = 0;

	/* change and configure coordination of tft */
	if (rotation == 0)
	{

	}
	else if (rotation == 1)
	{

	}
	else if (rotation == 2) // value by default is correct
	{

	}
	else if (rotation == 3)
	{
		SWAP(tp.x, tp.y);
		//SWAP(tp.x, tp.y);
		//tp.x = 1024 - tp.x ;
		//tp.y = 1024 - tp.y;
	}



	this->setCoordination(tp,rotation);

	BOXSIZE = tft.width() / 6;

	tft.fillScreen(BLACK);

	tft.fillRect(0, 0, BOXSIZE, BOXSIZE, RED);
	tft.fillRect(BOXSIZE, 0, BOXSIZE, BOXSIZE, YELLOW);
	tft.fillRect(BOXSIZE * 2, 0, BOXSIZE, BOXSIZE, GREEN);
	tft.fillRect(BOXSIZE * 3, 0, BOXSIZE, BOXSIZE, CYAN);
	tft.fillRect(BOXSIZE * 4, 0, BOXSIZE, BOXSIZE, BLUE);
	tft.fillRect(BOXSIZE * 5, 0, BOXSIZE, BOXSIZE, MAGENTA);
	tft.drawRect(0, 0, BOXSIZE, BOXSIZE, WHITE);
	currentcolor = RED;
	delay(1000);
	

	
  while (1) 
  {

	  // This part detect pressure and display informations on the screen
	  tp = ts.getPoint();
	  pinMode(XM, OUTPUT);
	  pinMode(YP, OUTPUT);
	  pinMode(XP, OUTPUT);
	  pinMode(YM, OUTPUT);
	
	  if (tp.z > MINPRESSURE && tp.z < MAXPRESSURE) 
	  {

		  switch (rotation) {      // adjust for different aspects
		  case 0:   break;        //no change,  calibrated for PORTRAIT
		  case 1:   tmp = TS_LEFT;
			  TS_LEFT = TS_BOT;
			  TS_BOT = TS_RIGHT;
			  TS_RIGHT = TS_TOP;
			  TS_TOP = tmp;
			  break;
		  case 2:   SWAP(TS_LEFT, TS_RIGHT);
			  SWAP(TS_TOP, TS_BOT);
			  break;
		  case 3:   tmp = TS_LEFT;
			  TS_LEFT = TS_BOT;
			  TS_TOP = TS_RIGHT;
			  TS_RIGHT = TS_TOP;
			  TS_BOT = tmp;
			  break;
		  }

		  xpos = map(tp.x, TS_LEFT, TS_RIGHT, 0, tft.width()); // ts_left = , ts_right = 240
		  ypos = map(tp.y, TS_TOP, TS_BOT, 0, tft.height()); // ts_top = 0 , ts_bot = 400

		 /* if (rotation == 0)
		  {
			  xpos = map(tp.x, TS_LEFT, TS_RIGHT, 0, tft.width()); // ts_left = , ts_right = 240
			  ypos = map(tp.y, TS_TOP, TS_BOT, 0, tft.height()); // ts_top = 0 , ts_bot = 400

			  tp.x = 1023 - tp.x; //  set x=0 at bottom in the left of the display
			  tp.y = 1023 - tp.y;

		  }
		  else if (rotation == 1)
		  {
			  xpos = map(tp.y, TS_TOP, TS_BOT, 0, tft.height()); //
			  ypos = map(tp.x, TS_LEFT, TS_RIGHT, 0, tft.width()); //	

			  tmp = tp.x;
			  tp.x = 1023 - tp.y;
			  tp.y = tmp;
		  }
		  else if (rotation == 2)
		  {
			  xpos = map(tp.x, TS_TOP, TS_BOT, 0, tft.height()); //
			  ypos = map(tp.y, TS_LEFT, TS_RIGHT, 0 , tft.width()); //	
		  }
		  else if (rotation == 3)
		  {
			 // xpos = map(tp.y, TS_LEFT, TS_RIGHT, tft.width(), 0); //
			 // ypos = map(tp.x, TS_TOP, TS_BOT,tft.height(),0 ); //

			  ypos = map(tp.x, TS_LEFT, TS_RIGHT, 0 , tft.height()); //
			  xpos = map(tp.y, TS_TOP, TS_BOT,0, tft.width()); //

			  tmp = tp.x;
			  tp.x = tp.y;
			  tp.y = 1023 - tmp;
		  
		  }*/

		 /* tft.setTextSize(2);
		  tft.setTextColor(YELLOW, BLACK);
		  tft.setCursor(0, (tft.height() * 3) / 4);		  
		  tft.print(number);
		  tft.setTextColor(YELLOW, BLACK);*/

		 // tft.print("tp.x=" + String(tp.x) + " tp.y=" + String(tp.y) + "   ");

		  printTFT(tft,0, (tft.height() * 3) / 4, 1, YELLOW, BLACK, (String)number);	
		  printTFT(tft, 0, (tft.height() * 3) / 4 - 40, 1, YELLOW, BLACK, "tp.x =" + (String)tp.x);
		  printTFT(tft, 0, (tft.height() * 3) / 4 - 30, 1, YELLOW, BLACK, "tp.y =" + (String)tp.y);

		  printTFT(tft, 0, (tft.height() * 3) / 4 - 20 , 1, YELLOW, BLACK, "width =" + (String)tft.width());
		  printTFT(tft, 0, (tft.height() * 3) / 4 - 10, 1, YELLOW, BLACK, "height =" + (String)tft.height());
		  printTFT(tft, 0, (tft.height() * 3) / 4 , 1, YELLOW, BLACK,"xpos =" + (String)xpos);
		  printTFT(tft, 0, (tft.height() * 3) / 4 + 10, 1, YELLOW, BLACK, "ypos =" + (String)ypos);

		  //printTFT(tft, 0, (tft.height() * 3) / 4 + 10, 1, YELLOW, BLACK, textTest);
		  //printTFT(tft, 6 * strlen(textTest), ((tft.height() * 3) / 4) + 10 , 1, YELLOW, BLACK, (String) numbTest );  // one character = 6 pixel, so add the value "numbtest" after textTest

		  if (ypos < BOXSIZE)
		  { 
			  //draw white border on selected color box
			  oldcolor = currentcolor;

			  if (xpos < BOXSIZE) {

				  currentcolor = RED;
				  tft.drawRect(0, 0, BOXSIZE, BOXSIZE, WHITE);
				  //if(number > 0) 
					  number--;
					  numbTest--;
			  }
			  else if (xpos < BOXSIZE * 2) {
				  currentcolor = YELLOW;
				  tft.drawRect(BOXSIZE, 0, BOXSIZE, BOXSIZE, WHITE);
			  }
			  else if (xpos < BOXSIZE * 3) {
				  currentcolor = GREEN;
				  tft.drawRect(BOXSIZE * 2, 0, BOXSIZE, BOXSIZE, WHITE);
			  }
			  else if (xpos < BOXSIZE * 4) {
				  currentcolor = CYAN;
				  tft.drawRect(BOXSIZE * 3, 0, BOXSIZE, BOXSIZE, WHITE);
			  }
			  else if (xpos < BOXSIZE * 5) {
				  currentcolor = BLUE;
				  tft.drawRect(BOXSIZE * 4, 0, BOXSIZE, BOXSIZE, WHITE);
			  }
			  else if (xpos < BOXSIZE * 6) {
				  currentcolor = MAGENTA;
				  tft.drawRect(BOXSIZE * 5, 0, BOXSIZE, BOXSIZE, WHITE);
				  number++;
				  numbTest++;
			  }

			  if (oldcolor != currentcolor) { //rub out the previous white border
				  if (oldcolor == RED) tft.fillRect(0, 0, BOXSIZE, BOXSIZE, RED);
				  if (oldcolor == YELLOW) tft.fillRect(BOXSIZE, 0, BOXSIZE, BOXSIZE, YELLOW);
				  if (oldcolor == GREEN) tft.fillRect(BOXSIZE * 2, 0, BOXSIZE, BOXSIZE, GREEN);
				  if (oldcolor == CYAN) tft.fillRect(BOXSIZE * 3, 0, BOXSIZE, BOXSIZE, CYAN);
				  if (oldcolor == BLUE) tft.fillRect(BOXSIZE * 4, 0, BOXSIZE, BOXSIZE, BLUE);
				  if (oldcolor == MAGENTA) tft.fillRect(BOXSIZE * 5, 0, BOXSIZE, BOXSIZE, MAGENTA);
			  }
		  }
	  }
  }

}

void Menu_adafruit::displayTFTln(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint8_t textSize, uint16_t color, uint16_t background , uint8_t rotation , const char *text)
{

  tft.setCursor(x,y);
  tft.setTextSize(textSize);
  tft.setTextColor(color);
  tft.println(text);  
  tft.setTextColor(color);

}

int16_t Menu_adafruit::getBoxSize(void)
{
  return BOXSIZE;
}

uint16_t Menu_adafruit::getIdentifier(MCUFRIEND_kbv tft)
{
  return identifier = tft.readID();
}


uint8_t Menu_adafruit::getOrientation(void)
{
  return Orientation;
}

void Menu_adafruit::setRotationTouchScreenXY(uint8_t rotation)
{
	uint16_t tmp;
	Orientation = rotation % 4;

	switch (Orientation) {      // adjust for different aspects
	case 0:   break;        //no change,  calibrated for PORTRAIT
	case 1:   tmp = TS_LEFT;
		TS_LEFT = TS_BOT;
		TS_BOT = TS_RIGHT;
		TS_RIGHT = TS_TOP;
		TS_TOP = tmp;
		break;
	case 2:   SWAP(TS_LEFT, TS_RIGHT);
		SWAP(TS_TOP, TS_BOT);
		break;
	case 3:   tmp = TS_LEFT;
		TS_LEFT = TS_TOP;
		TS_TOP = TS_RIGHT;
		TS_RIGHT = TS_BOT;
		TS_BOT = tmp;
		break;
	}

	ts = TouchScreen(XP, YP, XM, YM, 300);     //call the constructor AGAIN with new values.
}

void Menu_adafruit::initTFT(MCUFRIEND_kbv tft, uint8_t rotation,uint16_t background)
{

	tft.begin(9600);  // start 9600bauds serial connection
	tft.reset();  // reset the display tft
	identifier = tft.readID();
	tft.begin(identifier);  // start the display TFT
	tft.fillScreen(background); // background is black color
}

