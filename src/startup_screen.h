#pragma once
#ifndef _STARTUP_SCREEN_H
#define _STARTUP_SCREEN_H

#include "MCUFRIEND_kbv.h"
#include "Menu_adafruit.h"

#define VER_X 0
#define VER_Y 07
#define VER_Z 15

class startup_screen {

public:
	void display_logo(void);
	void display_startup_screen(MCUFRIEND_kbv tft, Menu_adafruit menu);
	void display_button(MCUFRIEND_kbv tft_lcd, Menu_adafruit menu_ada);
	int getNbModuleConnected(void);
	void setNbModuleConnected(uint8_t nb_module_connected);

private:
	uint8_t NbModuleConnect;

};


#endif
