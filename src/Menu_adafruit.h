#pragma once
#ifndef _MENU_ADAFRUIT_H
#define _MENU_ADAFRUIT_H

#include "Arduino.h"
#include "Adafruit_GFX.h"
#include "MCUFRIEND_kbv.h"
#include "TouchScreen.h"
#include <string.h>

#define SWAP(a, b) {uint16_t tmp = a; a = b; b = tmp;}

// Assign human-readable names to some common 16-bit color values:
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#define MINPRESSURE 20
#define MAXPRESSURE 1000



class Menu_adafruit: public MCUFRIEND_kbv {

public:

  Menu_adafruit(void);
  void displayTFT(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint8_t textSize, uint16_t color, uint16_t background, uint8_t rotation, const char *text);
  void displayTFTln(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint8_t textSize, uint16_t color, uint16_t background, uint8_t rotation, const char *text);
  int16_t getBoxSize(void);
  uint16_t getIdentifier(MCUFRIEND_kbv tft);
  uint8_t getOrientation(void);
  void setRotationTouchScreenXY(uint8_t rotation);
  void initTFT(MCUFRIEND_kbv tft, uint8_t rotation, uint16_t background);
  void show_Serial(MCUFRIEND_kbv tft);
  void setCoordination(TSPoint tp, uint8_t rotation);
  void getCoordination(long* output_xpos, long* output_ypos, MCUFRIEND_kbv tft, uint8_t rotation);
  void printTFT(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint8_t sizeText, uint16_t color, uint16_t background, String text);
  void printTFT(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint8_t sizeText, uint16_t color, uint16_t background, const char *text);
  void clearLine(MCUFRIEND_kbv tft, uint16_t x, uint16_t y, uint16_t width, uint8_t sizeText, uint16_t color);
  int16_t getIdentifier(void);

private:
  
  int16_t BOXSIZE;
  int16_t PENRADIUS = 3;
  uint16_t identifier, oldcolor, currentcolor;
  uint8_t Orientation = 0;    //PORTRAITS

  uint8_t SwapXY = 0;
  
  // most mcufriend shields use these pins and Portrait mode:
  uint8_t YP = A1;  // must be an analog pin, use "An" notation!
  uint8_t XM = A2;  // must be an analog pin, use "An" notation!
  uint8_t YM = 7;   // can be a digital pin
  uint8_t XP = 6;   // can be a digital pin

  
  uint16_t TS_LEFT;
  uint16_t TS_RIGHT;
  uint16_t TS_TOP;
  uint16_t TS_BOT;

  TouchScreen ts = TouchScreen(XP, YP, XM, YM, 250);
  TSPoint tp;
};

#endif //_MENU_DISPLAY_ADAFRUIT_H

