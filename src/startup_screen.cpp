#include "startup_screen.h"
#include "MCUFRIEND_kbv.h"
#include "Menu_adafruit.h"

void startup_screen::display_logo(void)
{
}

void startup_screen::display_startup_screen(MCUFRIEND_kbv tft_lcd, Menu_adafruit menu)
{
	menu.initTFT(tft_lcd,1, BLACK);

	tft_lcd.begin(menu.getIdentifier());
	tft_lcd.setRotation(1);
	menu.setRotationTouchScreenXY(1);
	tft_lcd.setCursor(0, 0);
	
	//Display dev by Fikouz
	menu.printTFT(tft_lcd, 10, 220, 1, YELLOW, BLACK, "Developed by FikOuZ");

	//Display version
	menu.printTFT(tft_lcd, 320, 220, 1, YELLOW, BLACK, "V " + (String)VER_X + "." + (String)VER_Y + "." + (String)VER_Z);

	//Display Title
	menu.printTFT(tft_lcd, 80, 40 , 4, YELLOW, BLACK, "Dev FikOuZ");
	for (int i = 0;i < 4;i++)
	{
		if (i == 0)
			menu.printTFT(tft_lcd, 160, 100, 2, YELLOW, BLACK, "Loading");
		else if (i == 1)
			menu.printTFT(tft_lcd, 160, 100, 2, YELLOW, BLACK, "Loading.");
		else if (i == 2)
			menu.printTFT(tft_lcd, 160, 100, 2, YELLOW, BLACK, "Loading..");
		else
			menu.printTFT(tft_lcd, 160, 100, 2, YELLOW, BLACK, "Loading...");

		delay(2000);
	}

	if (this->getNbModuleConnected() == 0)
		menu.printTFT(tft_lcd, 80, 100, 2, YELLOW, BLACK, "No module connected");
	else
		this->display_button(tft_lcd,menu);
}

void startup_screen::display_button(MCUFRIEND_kbv tft_lcd, Menu_adafruit menu_ada)
{
	long xpos_touchscreen;
	long ypos_touchscreen;

	menu_ada.clearLine(tft_lcd, 160, 100, 150, 2, BLACK); // delete Loading message
	menu_ada.printTFT(tft_lcd, 160, 120, 3, YELLOW, BLACK, "START");
	menu_ada.getCoordination(&xpos_touchscreen, &ypos_touchscreen, tft_lcd, 1);
}

int startup_screen::getNbModuleConnected(void)
{
	return NbModuleConnect;
}

void startup_screen::setNbModuleConnected(uint8_t nb_module_connected)
{
	NbModuleConnect = nb_module_connected;
}
